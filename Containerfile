FROM quay.io/toolbx-images/archlinux-toolbox

LABEL name="" \
      version="latest" \
      usage="This image is meant to run rocm in a container" \
      summary="Base image for creating Arch Linux rocm containers"

RUN pacman -Syu --noconfirm \
        bc \
        curl \
        findutils \
        less \
        ncurses \
        pinentry \
        shadow \
        util-linux \
        bash-completion \
        diffutils \
        git \
        gnupg \
        keyutils \
        lsof \
        man-db \
        man-pages \
        mlocate \
        mtr \
        nss-mdns \
        openssh \
        pigz \
        procps-ng \
        rsync \
        tcpdump \
        time \
        traceroute \
        tree \
        unzip \
        vte-common \
        wget \
        words \
        xorg-xauth \
        mesa \
        opengl-driver \
        vulkan-radeon \
        python \
        rocm-hip-runtime \
        hip-runtime-amd \
        miopen-hip \
        python-pip \
        clinfo \
        socat \
        rocm-opencl-runtime

ARG USERNAME=rocm
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && pacman -Sy --noconfirm sudo \
    && echo $USERNAME ALL='(ALL:ALL)' ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

RUN usermod -a -G video $USERNAME

WORKDIR /home/$USERNAME

USER $USERNAME

