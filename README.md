# ROCM-Container
_________________________________________

Arch based ROCM ready Podman container file

This is something i put together due to being unhappy with AMDs ROCM images because

AMDs ROCM/Pytorch docker image is a wopping 36GB so i wanted to try and make a smaller one

They require ROCM installed on the host which doesnt work for Silverblue very easily and isnt even needed.

I also wasnt happy with the run options they suggested as some seem to be unecessary holes in the containers security

*--security-opt seccomp=unconfined* isnt needed for ROCM to work

*--cap-add=SYS_PTRACE* also isnt needed for ROCM to work

*--ipc=host* this is not needed if the device is passed to the container properly

*--network=host* This can be worked around with a combination of socat and the *--publish* podman option

_________________________________________

**USAGE:**

This Guide assumes podman-desktop along with podman being setup in rootless mode, this hasnt been tested with root podman so YMMV

1. Download The Containerfile somehwere podman-desktop has access to

2. Open Podman-Desktop and go to *Images* and select *Build and Image* in the top right

3. In the *Build an Image* menu select the containerfile to build from and name the container then click *Build*
_____________

Now you will need to switch to a terminal as these steps cant be completed within podman-desktops UI

_____________

4. in terminal either use podman run or podman create to make the container I.E

    `podman create --name "nameofcontainer" -it --group-add=video --device=/dev/kfd --device=/dev/dri --shm-size=#g "imagename"`

5. If you used podman run you need to exit, with create and run you need to now start the container by going to *Containers* in podman-desktop and clicking the play button

6. In a terminal run `podman exec -u root -it "nameofcontainer"" /bin/bash` to enter a root prompt for the container

7. execute these commands within the container, you need to set a passwd for rocm user and i will not feel bad for you if you dont do this

    `passwd rocm`

    and disable root user in the container, not sure this really does much but why not?

    `passwd --lock root`

    now exit the root prompt of the container

8. After leaving the root prompt you can use the terminal in podman-desktop as the rocm user or run

    `podman exec -it "nameofcontainer" /bin/bash`
    to enter the container as rocm user

    You should avoid doing things as the root user in your container and it is setup for rocm user to be the default

9. Setup the ROCM stuff you wish to use, Arch instructions for setup should work in this container and youll be on your own for exacctly how to setup your specfic use case. The drivers are installed for ML,HIP,and OpenCL already

_____________

If you need to access the localhost or some other network resource within the container from the host you can use socat to accomplish this without --network=host

I.E If youre running Stable Diffusion WebUI you can create the container like this

`podman create --name "nameofcontainer" -it --publish="Desired IP on Host":"Host Port":"Desired Port in container" --group-add=video --device=/dev/kfd --device=/dev/dri --shm-size=#g "imagename"`

Then to run the webUI (after following the directions for its setup) run the webui like this

`socat  tcp-listen:"Desired Port in container",reuseaddr,fork tcp:127.0.0.1:7860 & ./webui.sh`

This will send the traffic from the containers localhost port 7860 to the port you passed to the host. If you mapped it to the same ip/port on the host you can now access webui normally in a browser

_____________

on silverblue or workstation Fedora you **NEED** to adjust SELinux permissions to allow the container to access the device, SELinux will not let you otherwise

`sudo setsebool -P container_use_devices=true`
